# nds3 conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/nds3"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS nds3 module
