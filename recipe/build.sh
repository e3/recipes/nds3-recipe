#!/bin/bash

# LIBVERSION shall only include MAJOR.MINOR.PATCH for require
LIBVERSION=$(echo ${PKG_VERSION}| cut -d'.' -f1-3)
E3_NDS3_LOCATION="${EPICS_MODULES}/${PKG_NAME}/${PKG_VERSION}"

# Remove EEE Makefile
rm GNUmakefile

# Clean between variants builds
make clean

make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install

# Headers should be under nds3 directory
# Done in e3-nds3/configure/module/RULES_MODULE
mkdir ${E3_NDS3_LOCATION}/include/nds3
mv ${E3_NDS3_LOCATION}/include/*.h ${E3_NDS3_LOCATION}/include/nds3/
